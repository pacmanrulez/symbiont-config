<?php

namespace Symbiont\Config\Exceptions;

class FileNotFoundException extends Exception {
    public function __construct(string $file) {
        parent::__construct(sprintf('file `%s` not found', $file));
    }
}