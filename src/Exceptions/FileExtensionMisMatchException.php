<?php

namespace Symbiont\Config\Exceptions;

class FileExtensionMisMatchException extends Exception {
    public function __construct(string $file, string $extension) {
        parent::__construct(sprintf('File `%s` should contain `%s` extension', $file, $extension));
    }
}