<?php

namespace Symbiont\Config\Exceptions;

class ArrayPathNotFoundException extends Exception {
    public function __construct(string $path) {
        parent::__construct(sprintf('Array path %s not found', $path));
    }
}