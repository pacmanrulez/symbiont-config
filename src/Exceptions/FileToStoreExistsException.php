<?php

namespace Symbiont\Config\Exceptions;

class FileToStoreExistsException extends Exception {
    public function __construct(string $file) {
        parent::__construct(sprintf('File `%s` already exists, use `save()` to store current configuration', $file));
    }
}