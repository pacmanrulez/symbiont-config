<?php

namespace Symbiont\Config\Exceptions;

class ArrayDriverExpectsArrayException extends Exception {
    public function __construct(string $file, string $type) {
        parent::__construct(sprintf('File `%s` should return an array, `%s` given', $file, $type));
    }
}