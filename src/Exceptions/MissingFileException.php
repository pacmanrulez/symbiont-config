<?php

namespace Symbiont\Config\Exceptions;

class MissingFileException extends Exception {
    public function __construct() {
        parent::__construct('File was not specified, use `store($file)` instead');
    }
}