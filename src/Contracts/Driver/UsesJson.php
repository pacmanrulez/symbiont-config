<?php

namespace Symbiont\Config\Contracts\Driver;

interface UsesJson {

    public function fromJson(string $values, int $flags = null, int $depth = null): null|array;
    public function toJson(array $values, int $flags = null): false|string;

}