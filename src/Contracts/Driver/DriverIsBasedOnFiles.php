<?php

namespace Symbiont\Config\Contracts\Driver;

interface DriverIsBasedOnFiles {

    public static function getFileExtension(): string;
    public function validate(string $file): bool;
    public function load(string $file): mixed;
    public function save(string $file, array $values): bool;
    public function stored(string $file): bool;
    public function store(string $file, array $values): bool;
    public function unlink(string $file): bool;

}