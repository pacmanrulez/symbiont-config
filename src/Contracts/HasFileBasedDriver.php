<?php

namespace Symbiont\Config\Contracts;

interface HasFileBasedDriver {

    public function load(string $file): Configurable;
    public function save(): bool;
    public function stored(): bool;
    public function store(string $file): bool;
    public function unlink(): bool;

}