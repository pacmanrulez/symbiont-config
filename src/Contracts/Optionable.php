<?php

namespace Symbiont\Config\Contracts;

interface Optionable {

    public function getOptions(): array;
    public function getFlags(): array;
    public function hasOption(string $key): bool;
    public function hasFlag(int|string $value): bool;
    public function removeOption(string $key): void;
    public function option(string $key, mixed $default = null, bool $remove = false): mixed;
    public function flag(int|string $value, bool $default = false): bool;

}