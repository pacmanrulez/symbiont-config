<?php

namespace Symbiont\Config\Contracts;

use Symbiont\Config\Contracts\Driver\DrivesConfig;

interface IsDriverBased {

    public function setDriver(DrivesConfig $driver): IsDriverBased;
    public function getDriver(): DrivesConfig;
    public function forwardDriver(): array;

}