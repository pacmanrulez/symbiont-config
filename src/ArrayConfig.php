<?php

namespace Symbiont\Config;

use Symbiont\Support\ForwardCall\Contracts\ForwardsCalls;

use Symbiont\Config\Concerns\{HandleDriverBasedConfig, HandleFileStorage, HandlesOptions};
use Symbiont\Config\Contracts\{HasFileBasedDriver, IsDriverBased};

/**
 * @method on(string $event, \Closure $callback)
 * @method once(string $event, \Closure $callback)
 * @method off(string $event)
 * @method offAll()
 */
class ArrayConfig extends Config
    implements
        IsDriverBased,
        HasFileBasedDriver,
        ForwardsCalls {

    use HandleDriverBasedConfig,
        HandleFileStorage;

    public function __construct(string $file = null, array $options = []) {
        $this->bootTraits([
            HandlesOptions::class => [
                'options' => $options
            ],
            HandleDriverBasedConfig::class => [
                'driver' => new Drivers\ArrayConfigDriver($options['driver'] ?? []),
                'callbacks' => $options['callbacks'] ?? []
            ],
            HandleFileStorage::class => [
                'file' => $file
            ]
        ], [
            HandlesOptions::class,
            HandleDriverBasedConfig::class,
            HandleFileStorage::class
        ]);

        parent::__construct($options);
    }

    public function forwardDriver(): array {
        return ['on', 'once', 'off', 'offAll'];
    }

    /**
     * Make trigger protected without the need to forward this method
     * @param string $event
     * @param array $arguments
     * @return mixed
     */
    protected function trigger(string $event, array $arguments = []) {
        return $this->driver->trigger($event, $arguments);
    }

}