<?php

namespace Symbiont\Config;

use Symbiont\Config\Concerns\HandleDriverBasedConfig;
use Symbiont\Config\Concerns\HandlesOptions;
use Symbiont\Config\Contracts\Configurable;

class JsonConfig extends ArrayConfig {

    public function __construct(string $file = null, array $options = []) {
        $this->bootTraits([
            HandlesOptions::class => [
                'options' => $options
            ],
            HandleDriverBasedConfig::class => [
                'driver' => new Drivers\JsonConfigDriver($options['driver'] ?? []),
                'callbacks' => $options['callbacks'] ?? []
            ]
        ], [
            HandlesOptions::class,
            HandleDriverBasedConfig::class
        ]);

        parent::__construct($file, $options);
    }

    public function fromJson(string $values, int $flags = null, int $depth = null): Configurable {
        $this->values = $this->driver->fromJson($values, $flags, $depth) ?? [];
        return $this;
    }

    public function toJson(int $flags = null): false|string {
        return $this->driver->toJson($this->values, $flags);
    }

}