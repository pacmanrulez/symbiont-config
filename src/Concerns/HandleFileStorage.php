<?php

namespace Symbiont\Config\Concerns;

use Symbiont\Config\Contracts\Configurable;
use Symbiont\Config\Drivers\FileDriver;
use Symbiont\Config\Exceptions\FileToStoreExistsException;
use Symbiont\Config\Exceptions\MissingDriverException;
use Symbiont\Config\Exceptions\MissingFileException;

trait HandleFileStorage {

    protected ?string $file = null;

    protected function initializeHandleFileStorage(?string $file = null): void {
        if($file) {
            $this->file = $file;

            if($this->stored()) {
                if($this->flag(FileDriver::OPTION_DO_NOT_LOAD_FILE)) {
                    return;
                }
            }
            else {
                if($this->flag(FileDriver::OPTION_CREATE_FILE)) {
                    $this->store($file);
                }
                if($this->flag(FileDriver::OPTION_ALLOW_WITHOUT_FILE)) {
                    return;
                }
            }

            $this->load($file);
        }
    }

    public function getFile(): null|string {
        return $this->file;
    }

    public function load(string $file): Configurable {
        if(! $this->driver) {
            throw new MissingDriverException;
        }

        $this->values = $this->driver->load($file);
        $this->file = $file;

        return $this;
    }

    public function save(): bool {
        if(! $this->driver) {
            throw new MissingDriverException;
        }

        if(! $this->file) {
            throw new MissingFileException;
        }

        return $this->driver->save($this->file, $this->values);
    }

    public function stored(): bool {
        if(! $this->driver) {
            throw new MissingDriverException;
        }

        if($this->file && $stored = $this->driver->stored($this->file)) {
            return $stored;
        }

        return false;
    }

    public function store(string $file): bool {
        if(! $this->driver) {
            throw new MissingDriverException;
        }

        if(file_exists($file)) {
            throw new FileToStoreExistsException($file);
        }

        if($this->driver->store($file, $this->values)) {
            $this->file = $file;
            return true;
        }

        return false;
    }

    public function unlink(): bool {
        if(! $this->driver) {
            throw new MissingDriverException;
        }

        if(! $this->file) {
            throw new \Exception('Unable to delete, no file specified.' );
        }

        return $this->driver->unlink($this->file);
    }

    public function saveFile() {
        return $this->driver->saveFile($this->file, $this->values);
    }

}