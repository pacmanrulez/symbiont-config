<?php

namespace Symbiont\Config\Concerns\Driver;

trait HandleJson {

    public static int $json_depth = 8;
    public static int $json_enc_flags = JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT;
    public static int $json_dec_flags = 0;

    public static function getFileExtension(): string {
        return 'json';
    }

    public function fromJson(string $values, int $flags = null, int $depth = null): null|array {
        return json_decode($values, true, $depth ?? self::$json_depth,  $flags ?? self::$json_dec_flags);
    }

    public function toJson(array $values, int $flags = null): false|string {
        return json_encode($values, $flags ?? self::$json_enc_flags);
    }

}