<?php

namespace Symbiont\Config\Concerns\Driver;

use Symbiont\Config\ConfigSettings;
use Symbiont\Config\Contracts\Configurable;
use Symbiont\Config\Exceptions\FileExtensionMisMatchException;
use Symbiont\Config\Exceptions\FileNotFoundException;
use Symbiont\Config\Exceptions\FileToStoreExistsException;
use Symbiont\Dispatcher\Concerns\DispatchesEvent;

trait HandleFileBasedDrivers {

    use DispatchesEvent;

    const CLB_LOADING = 'loading';
    const CLB_LOADED = 'loaded';
    const CLB_SAVING = 'saving';
    const CLB_SAVED = 'saved';
    const CLB_STORING = 'storing';
    const CLB_STORED = 'stored';
    const CLB_CREATING = 'creating';
    const CLB_CREATED = 'created';
    const CLB_UNLINKING = 'unlinking';
    const CLB_UNLINKED = 'unlinked';

    public static function getFileExtension(): string {
        return 'syckdev';
    }

    public static function from(string $file, array $options = []): Configurable {
        return ConfigSettings::from($file, $options);
    }

    // throws exception
    protected function throwInvalid(string $file): void {
        throw new FileExtensionMisMatchException($file, static::getFileExtension());
    }

    public function validateFile(string $file): void {
        if($this->validate($file) === false) {
            $this->throwInvalid($file);
        }
    }

    public function validate(string $file): bool {
        return pathinfo($file, PATHINFO_EXTENSION) === static::getFileExtension();
    }

    public function save(string $file, array $values): bool {
        $this->trigger(static::CLB_SAVING);
        $result = $this->saveFile($file, $values);
        $this->trigger(static::CLB_SAVED);
        return $result;
    }

    public function load(string $file): array {
        $this->validateFile($file);

        $this->trigger(static::CLB_LOADING, [$file]);

        if($this->stored($file)) {
            $result = $this->loadFile($file);
            $this->trigger(static::CLB_LOADED, [$file, $result]);
            return $result;
        }

        throw new FileNotFoundException($file);
    }

    public function stored(string $file): bool {
        return file_exists($file);
    }

    public function store(string $file, array $values): bool {
        $this->validateFile($file);
        $stored = $this->stored($file);

        if(! $stored) {
            $this->trigger(static::CLB_CREATING, [$file]);
        }
        $this->trigger(static::CLB_STORING, [$file, $values]);
        if($stored) {
            throw new FileToStoreExistsException($file);
        }

        $result = $this->saveFile($file, $values);
        $this->trigger(static::CLB_STORED, [$file, $values, $result]);

        if(! $stored) {
            $this->trigger(static::CLB_CREATED, [$file]);
        }

        return $result;
    }

    public function unlink(string $file): bool {
        $this->trigger(static::CLB_UNLINKING, [$file]);
        $result = @unlink($file);
        $this->trigger(static::CLB_UNLINKED, [$file, $result]);
        return $result;
    }

    // required to be implemented by each file based driver
    abstract protected function saveFile(string $file, array $values);
    abstract protected function loadFile(string $file);

}