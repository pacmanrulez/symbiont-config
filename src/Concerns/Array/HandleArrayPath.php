<?php

namespace Symbiont\Config\Concerns\Array;

use Symbiont\Config\ConfigSettings;
use Symbiont\Config\Contracts\Configurable;
use Symbiont\Config\Exceptions\AddOnlyAllowedOnArraysException;
use Symbiont\Config\Exceptions\ArrayPathNotFoundException;

trait HandleArrayPath {

    public string $xpath_delimiter;
    public bool $xpath_throw_exception;

    protected function initializeHandleArrayPath() {
        $this->xpath_delimiter = ConfigSettings::$xpath_delimiter;
        $this->xpath_throw_exception = ConfigSettings::$xpath_throw_exception;
    }

    public function get(string|array $key = null, mixed $default = null) {
        if(! $key) {
            return $this->values;
        }

        if(! $this->keyIsPath($key)) {
            return parent::get($key, $default);
        }

        return $this->getByPath($key, $this->values, $default);
    }

    public function set(string|array $key, mixed $value): Configurable {
        if(! $this->keyIsPath($key)) {
            return parent::set($key, $value);
        }

        return $this->setByPath($key, $this->values, $value);
    }

    public function unset(string|array $key): Configurable {
        if(! $this->keyIsPath($key)) {
            return parent::unset($key);
        }

        return $this->unsetByPath($key, $this->values);
    }

    public function add(string|array $key, mixed $value, int $flags = 0): Configurable {
        if(! $this->keyIsPath($key)) {
            return parent::add($key, $value, $flags);
        }

        return $this->addByPath($key, $this->values, $value, $flags);
    }

    public function remove(string|array $key, mixed $value): Configurable {
        if(! $this->keyIsPath($key)) {
            return parent::remove($key, $value);
        }

        return $this->removeByPath($key, $this->values, $value);
    }

    public function keyIsPath(string|array $key): bool {
        return is_string($key) ?
            strpos($key, $this->xpath_delimiter) !== false :
            ($key ? true : false);
    }

    public function extractKeyPath(string|array $key): array {
        return is_string($key)  ?
            explode($this->xpath_delimiter, trim($key)) :
            $key;
    }

    public function getByPath(string|array $key, array $values, mixed $default = null): mixed {
        $paths = $this->extractKeyPath($key);

        foreach($paths as $path) {
            if(is_array($values) && array_key_exists($path, $values)) {
                $values = $values[$path];
            }
            else {
                if($default) {
                    return $default;
                }

                return $this->pathNotFound($key, null);
            }
        }

        return $values;
    }

    // public static function setNestedElement($array, $keys, $value)
    public function setByPath(string|array $key, array &$values, mixed $value): Configurable {
        $paths = $this->extractKeyPath($key);

        $temp = &$values;
        for($index = 0, $many = count($paths); $index < $many; $index++) {
            if (! is_array($temp)) {
                if(($many-$index) > 0 && $this->xpath_throw_exception) {
                    return $this->pathNotFound($key, $this);
                }
                $temp = [];
            }
            $temp = &$temp[$paths[$index]];
        }
        $temp = $value;

        return $this;
    }

    public function unsetByPath(string|array $key, array &$values): Configurable {
        $paths = $this->extractKeyPath($key);

        $last = array_pop($paths);

        $temp = &$values;
        for($index = 0, $many = count($paths); $index < $many; $index++) {
            $path = $paths[$index];

            if (! is_array($temp)) {
                if(($many-$index) > 0 && $this->xpath_throw_exception) {
                    return $this->pathNotFound($key, $this);
                }

                throw new \Exception('throw some exception');
            }

            $temp = &$temp[$path];
        }

        if(! $temp || !array_key_exists($last, $temp)) {
            throw new ArrayPathNotFoundException($key);
        }

        unset($temp[$last]);

        return $this;
    }

    public function addByPath(string|array $key, array &$values, mixed $value, int $flags = 0): Configurable {
        $paths = $this->extractKeyPath($key);

        $temp = &$values;
        for($index = 0, $many = count($paths); $index < $many; $index++) {
            if(! is_array($temp)) {
                if( ($many-$index) > 0 &&
                    !($flags & self::ADD_IF_NOT_EXISTS) &&
                    $this->xpath_throw_exception) {
                        return $this->pathNotFound($key, $this);
                }
                $temp = [];
            }

            $temp = &$temp[$paths[$index]];
        }

        if(! $temp) {
            if($flags & self::ADD_IF_NOT_EXISTS) {
                $temp = $value;
                return $this;
            }
            if ($this->xpath_throw_exception) {
                return $this->pathNotFound($key, $this);
            }
            return $this;
        }

        if(! is_array($temp)) {
            if($flags & self::ADD_ONLY_ARRAYS) {
                throw new AddOnlyAllowedOnArraysException(gettype($temp));
            }

            $temp = [$temp];
        }

        $temp = array_merge(is_array($temp) ? $temp : [$temp], is_array($value) ? $value : [$value]);

        return $this;
    }

    public function removeByPath(string|array $key, array &$values, mixed $value, int $flags = 0): false|Configurable {
        $paths = $this->extractKeyPath($key);

        $temp = &$values;
        for($index = 0, $many = count($paths); $index < $many; $index++) {
            $path = $paths[$index];

            if (! is_array($temp)) {
                if(($many-$index) > 0 && $this->xpath_throw_exception) {
                    return $this->pathNotFound($key, $this);
                }

                throw new \Exception('throw some exception');
            }

            $temp = &$temp[$path];
        }

        if(! is_array($temp)) {
            throw new \Exception('throw some exception');
        }


        foreach(is_array($value) ? $value : [$value] as $value) {
            if (($location = array_search($value, $temp)) !== false) {
                unset($temp[$location]);
                // reset index
                $temp = array_values($temp);
            }
        }

        return $this;
    }

    protected function pathNotFound(string|array $key, mixed $return)  {
        $path = is_array($key) ?
            implode($this->xpath_delimiter, $key) :
            $key;
        return $this->xpath_throw_exception ?
            throw new ArrayPathNotFoundException($path)
            : $return;
    }
}