<?php

namespace Symbiont\Config\Concerns;

use Symbiont\Config\Contracts\Optionable;

trait HandlesOptions {

    protected array $options = [];

    protected function initializeHandlesOptions(array $options = []) {
        $this->options = $options;
    }

    public function flag(int|string $value, bool $default = false): bool {
        return in_array($value, $this->options, true) ?
            true : $default;
    }

    public function option(string $key, mixed $default = null, bool $remove = false): mixed {
        if(array_key_exists($key, $this->options)) {
            $value = $this->options[$key];
            if($remove) {
                unset($this->options[$key]);
            }
            return $value;
        }

        return $default;
    }

    public function removeOption(int|string $key): void {
        if(!is_numeric($key) && array_key_exists($key, $this->options)) {
            unset($this->options[$key]);
        }
        if(is_numeric($key) && ($index = array_search($key, $this->options)) !== false) {
            unset($this->options[$index]);
        }
    }

    public function hasFlag(int|string $value): bool {
        return in_array($value, $this->options, true);
    }

    public function getFlags(): array {
        return array_filter($this->options, function($key) {
            return is_integer($key);
        }, ARRAY_FILTER_USE_KEY);
    }

    public function hasOption(string $key): bool {
        return array_key_exists($key, $this->options);
    }

    public function getOptions(): array {
        return $this->options;
    }

    public function setOptions(array $options): Optionable  {
        $this->options = $options;
        return $this;
    }

}