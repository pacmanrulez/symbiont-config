<?php

namespace Symbiont\Config\Drivers;

use Symbiont\Config\Contracts\Driver\ConnectsToDatabase;

abstract class DatabaseDriver extends ConfigDriver implements ConnectsToDatabase {

    protected mixed $connection;

    protected string $field = 'config';

    protected array $options;

    abstract protected function validateOptions(array $options): array;

    public function __construct(array $options) {
        $this->options = $this->validateOptions($options);
        $this->connect();
    }

    public function __destruct() {
        $this->disconnect();
    }

    abstract function row($target, $field, $table): mixed;

}