<?php

namespace Symbiont\Config;

use Symbiont\Config\Contracts\Configurable;
use Symbiont\Config\Exceptions\FileNotFoundException;

class Config extends BaseConfig implements Contracts\Array\ArrayPaths {

    use Concerns\Array\HandleArrayPath;

    public function __construct(array $options = []) {
        parent::__construct($options);
    }

    public static function from(string $file, array $options = []): Configurable {
        $extension = pathinfo($file, PATHINFO_EXTENSION);
        if(! empty($extension) && $class = ConfigSettings::$drivers[$extension] ?? null) {
            return new $class($file, $options);
        }

        foreach(ConfigSettings::$drivers as $extension => $class) {
            if(file_exists($file_extended = $file . '.' . $extension)) {
                return new $class($file_extended, $options);
            }
        }

        throw new FileNotFoundException($file);
    }
}