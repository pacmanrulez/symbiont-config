<?php 

 return [
  'test' => 'tested',
  'array' => 
  [
    0 => 'a',
    1 => 'b',
    2 => 'c',
  ],
  'number' => 101,
  'nested' => 
  [
    'content' => 'available',
    'number' => 102,
  ],
  'deeply' => 
  [
    'nested' => 
    [
      0 => 'values',
    ],
  ],
  'stack' => 
  [
    0 => 'value-one',
  ],
  'testing' => 'save',
];