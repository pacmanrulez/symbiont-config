<?php

namespace Symbiont\Config\Tests\unit;

use PHPUnit\Framework\TestCase;
use Symbiont\Config\Config;
use Symbiont\Config\Contracts\Configurable;
use Symbiont\Config\Exceptions\MissingDriverException;

final class ConfigTest extends TestCase {

    protected Configurable $config;

    protected array $values = [
        'some' => 'values',
        'being' => 'set'
    ];

    protected array $values_nested = [
        'some' => [
            'nested',
            'values'
        ]
    ];

    protected function setUp(): void {
        // defaults to ArrayConfigDriver
        $class = new class extends Config {};
        $this->config = new $class;
    }

    public function testEmpty() {
        $this->assertEmpty($this->config->values());
    }

    public function testValues() {
        $this->config->values($this->values);

        $this->assertEquals($this->values, $this->config->values());
    }

    public function testSetValues() {
        $this->config->values($this->values);

        $this->assertEquals($this->values, $this->config->values());
        $this->assertEquals($this->values, $this->config->get());
    }

    public function testGet() {
        $this->config->values($this->values);

        $this->assertEquals('values', $this->config->get('some'));
    }

    public function testGetDefault() {
        $default = 'default-value';

        $this->assertEquals($default, $this->config->get('key-does-not-exist', $default));
    }

    public function testSet() {
        $this->config->values($this->values);
        $key = 'some';
        $value = 'value';

        $this->assertInstanceOf(Configurable::class, $this->config->set($key, $value));

        $values = $this->values;
        $values[$key] = $value;

        $this->assertEquals($values, $this->config->values());
    }

    public function testUnset() {
        $this->config->values($this->values);
        $this->config->unset('some');
        $this->assertEquals(['being' => 'set'], $this->config->values());
    }

    public function testAdd() {
        $this->config->values($this->values);
        $this->config->add('another' , 'thing');
        $this->assertEquals(array_merge($this->values, [
            'another' => ['thing']
        ]), $this->config->values());

        $this->config->values($this->values);
        $this->config->add('another' , ['thing']);
        $this->assertEquals(array_merge($this->values, [
            'another' => ['thing']
        ]), $this->config->values());

        $this->config->add('another', ['thingy']);
        $this->assertEquals(array_merge($this->values, [
            'another' => ['thing', 'thingy']
        ]), $this->config->values());

        $this->config->values($this->values_nested);
        $this->config->add('some', 'added');
        $this->assertEquals([
            'some' => array_merge($this->values_nested['some'], ['added'])
        ], $this->config->values());
    }

    public function testRemove() {
        $this->config->values($this->values_nested);
        $this->config->remove('some', 'nested');
        $this->assertEquals([
            'some' => [
                'values'
            ]
        ], $this->config->values());
    }

}