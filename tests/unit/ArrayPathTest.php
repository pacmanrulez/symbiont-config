<?php

namespace Symbiont\Config\Tests\unit;

use PHPUnit\Framework\TestCase;
use Symbiont\Config\Config;
use Symbiont\Config\ArrayConfig;
use Symbiont\Config\ConfigSettings;
use Symbiont\Config\Contracts\Configurable;
use Symbiont\Config\Exceptions\ArrayPathNotFoundException;

final class ArrayPathTest extends TestCase {

    protected string $file = './tests/tmp/.test-config.php';
    protected Configurable $config;

    protected function setUp(): void {
        $this->config = new ArrayConfig($this->file);
        $this->assertNotEmpty($this->config->values());
    }

    public function testGetPath() {
        $this->assertEquals('available', $this->config->get('nested->content'));
    }

    public function testGetPathDefault() {
        $this->assertEquals('should-default', $this->config->get('nested->content->none', 'should-default'));
    }

    public function testGetPathChangedDelimiter() {
        $this->config->xpath_delimiter = '=>';
        $this->assertEquals('available', $this->config->get('nested=>content'));
    }

    // should equally work for `set` and `add`
    public function testGetPathChangedGlobalDelimiter() {
        // set delimiter
        ConfigSettings::$xpath_delimiter = '..';
        // config needs to be created after global delimiter was changed
        $this->config = new ArrayConfig($this->file);
        $this->assertEquals('available', $this->config->get('nested..content'));
        // set delimiter back, else config object created after this test will fail with default delimiter
        ConfigSettings::$xpath_delimiter = ConfigSettings::XPATH_DELIMITER;
    }

    public function testGetArrayPathNotFoundException() {
        $this->expectException(ArrayPathNotFoundException::class);
        $this->config->get('nested->value->does->not->exist');
    }

    public function testGetArrayNoneExistingPath() {
        $this->config->xpath_throw_exception = false;
        $this->assertNull($this->config->get('nested->value->does->not->exist'));
    }

    public function testSetPath() {
        $this->config->set('nested->content', 'unavailable');
        $this->assertEquals('unavailable', $this->config->get('nested->content'));
    }

    public function testSetPathChangedDelimiter() {
        $this->config->xpath_delimiter = '=>';
        $this->config->set('nested=>content', 'unavailable');
        $this->assertEquals('unavailable', $this->config->get('nested=>content'));
    }

    public function testSetPathCreation() {
        $this->config->xpath_throw_exception = false;
        $this->config->set('this-path->does->not->exist', null);
        $this->assertEquals([
            'does' => [
                'not' => [
                    'exist' => null
                ]
            ]
        ], $this->config->get('this-path'));
    }

    public function testAdd() {
        $add = ['added' => 'value'];
        $current = $this->config->get('nested');
        $expected = array_merge($current, $add);

        $this->config->add('nested', $add);
        $this->assertEquals($expected, $this->config->get('nested'));
    }

    public function testAddByPath() {
        $add = ['added'];

        $expected = $this->config->get('deeply');
        $expected['nested'] = array_merge($expected['nested'], $add);

        $this->config->add('deeply->nested', $add);
        $this->assertEquals($expected, $this->config->get('deeply'));
    }

    public function testAddPathNotFoundException() {
        $this->expectException(ArrayPathNotFoundException::class);
        $this->config->set('nested->value->does->not->exist', 'exception-thrown');
    }

    public function testAddArrayNoneExistingPath() {
        $this->config->values([]);
        $this->config->add('nested->value->does->not->exist', 'bla', Config::ADD_IF_NOT_EXISTS);
        $this->assertEquals([
            'nested' => [
                'value' => [
                    'does' => [
                        'not' => [
                            'exist' => 'bla'
                        ]
                    ]
                ]
            ]
        ], $this->config->values());
    }
}