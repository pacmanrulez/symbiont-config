<?php

namespace Symbiont\Config\Tests\unit;

use PHPUnit\Framework\TestCase;
use Symbiont\Config\Concerns\Driver\HandleFileBasedDrivers;
use Symbiont\Config\Concerns\HandleDriverBasedConfig;
use Symbiont\Config\Concerns\HandleFileStorage;
use Symbiont\Config\BaseConfig;
use Symbiont\Config\Contracts\Configurable;
use Symbiont\Config\Contracts\Driver\DriverIsBasedOnFiles;
use Symbiont\Config\Contracts\Driver\DrivesConfig;
use Symbiont\Config\Contracts\IsDriverBased;
use Symbiont\Config\Drivers\ConfigDriver;
use Symbiont\Config\Drivers\FileDriver;
use Symbiont\Config\Exceptions\FileNotFoundException;
use Symbiont\Config\Exceptions\MissingDriverException;

/**
 * @todo: rewrite using BootsTrait/bootTraits
 */
final class DriverlessExceptionTest extends TestCase {

    protected Configurable $config;
    protected DrivesConfig $fake_driver;

    protected string $file = './tests/tmp/.test-config.syckdev';

    protected array $values = [
        'some' => 'values',
        'being' => 'set'
    ];

    protected function setUp(): void {
        // defaults to ArrayConfigDriver
        $class = new class extends BaseConfig implements IsDriverBased  {

            use HandleDriverBasedConfig,
                HandleFileStorage;

            protected function loadFile(string $file) {
                // TODO: Implement loadFile() method.
            }

            protected function saveFile(string $file, array $values) {
                // TODO: Implement saveFile() method.
            }

            public function validate(string $file): bool {
                return true;
            }
        };
        $this->config = new $class;

        $fake_driver = new class extends ConfigDriver implements DriverIsBasedOnFiles{
            use HandleFileBasedDrivers;

            protected function saveFile(string $file, array $values): void {
                // TODO: Implement saveFile() method.
            }

            protected function loadFile(string $file): void {
                // TODO: Implement loadFile() method.
            }
        };
        $this->fake_driver = new $fake_driver;
    }

    public function testLoadMissingDriverException() {
        $this->expectException(MissingDriverException::class);
        $this->config->load('something.'.FileDriver::getFileExtension());
    }

    public function testLoadFileNotFoundException() {
        $this->expectException(FileNotFoundException::class);
        $this->config->setDriver($this->fake_driver);
        $this->config->load('something.'.FileDriver::getFileExtension());
    }

    public function testSaveException() {
        $this->expectException(MissingDriverException::class);
        $this->config->save();
    }

    public function testStoreException() {
        $this->expectException(MissingDriverException::class);
        $this->config->store('something.'.FileDriver::getFileExtension());
    }

    public function testStoredException() {
        $this->expectException(MissingDriverException::class);
        $this->config->stored();
    }

    public function testUnlinkException() {
        $this->expectException(MissingDriverException::class);
        $this->config->unlink();
    }

}
