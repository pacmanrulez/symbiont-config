<?php

namespace Symbiont\Config\Tests\unit;

use PHPUnit\Framework\TestCase;
use Symbiont\Config\Config;
use Symbiont\Config\ArrayConfig;
use Symbiont\Config\ConfigSettings;
use Symbiont\Config\Drivers\ArrayConfigDriver;
use Symbiont\Config\Drivers\JsonConfigDriver;
use Symbiont\Config\JsonConfig;

final class AutoConfigTest extends TestCase {

    protected string $file_php = './tests/tmp/.test-config.php';
    protected string $file_json = './tests/tmp/.test-config.json';
    protected string $file_prio = './tests/tmp/.test-config';

    public function setUp(): void {
    }

    public function testAutoCreateArrayConfig() {
        $this->assertInstanceOf(ArrayConfig::class, Config::from($this->file_php));
    }

    public function testAutoCreateJsonConfig() {
        $this->assertInstanceOf(JsonConfig::class, Config::from($this->file_json));
    }

    public function testAutoCreatePriorityConfigAsJson() {
        $this->assertInstanceOf(JsonConfig::class, Config::from($this->file_prio));
    }

    public function testAutoCreatePriorityConfigAsArray() {
        // changing prio
        ConfigSettings::$drivers = [
            ArrayConfigDriver::getFileExtension() => ArrayConfig::class,
            JsonConfigDriver::getFileExtension() => JsonConfig::class
        ];
        $this->assertInstanceOf(ArrayConfig::class, Config::from($this->file_php));
    }
}