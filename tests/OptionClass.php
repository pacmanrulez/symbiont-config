<?php

namespace Symbiont\Config\Tests;

use Symbiont\Config\Concerns\HandlesOptions;
use Symbiont\Config\Contracts\Optionable;

class OptionClass implements Optionable {

    use HandlesOptions;

    public function __construct(array $options = []) {
        $this->setOptions($options);
    }

    public function setOptionsPublic(array $options): Optionable{
        return $this->setOptions($options);
    }

}