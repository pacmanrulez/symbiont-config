# Symbiont / Config

Simple config library.

This package is work in progress!

## Requirements

- PHP `8.2`

## Installation

```bash
composer require symbiont/config
```

## Usage

Using a general `Config` without a driver.

```php
use Symbiont\Config\Config;

$config = new Config;
$config->set('key', 'value');
$config->get('key');
```

Using preconfigured `ArrayConfig`

```php
// some-config.php
return [
    'nothing' => null,
    'some' => 'string',
    'and' => [
        'nested' => 'value'   
    ],
    'array' => [
        'with',
        'values'    
    ]
]
```

```json
{
  "nothing": null, 
  "some": "string",
  "and": {
    "nested": "value"
  },
  "array": [
    "with",
    "values"
  ]
}
```

```php
use Symbiont\Config\{Config, ArrayConfig, JsonConfig};

// as array
$config = new ArrayConfig('some-config.php');
// as json
$config = new JsonConfig('some-config.json');
// auto choose
$config = Config::from('some-config.json'); // returns JsonConfig object

$config->get('some') // `string`
$config->get('nothing', 'default-value') // `default-value`
$config->get('and->nested') // `value`
$config->set('and->nested', 'changed') // and->nested = `changed`
$config->set(['and', 'nested'], 'changed-again') // and->nested = `changed-again`
$config->add('array', 'added') // 'array' => ['with', 'values', 'changed']
$config->remove('array', 'values') // 'array' => ['with', 'changed']
$config->unset('some')
$config->save() // save changes to file
$config->store('new-file') // stores current config to new file

// @todo:
// $config->storeVersion() // stores a version of a config leaving the original untouched
// $config->loadVersion($version) // load a stored `$version` of the original config
```

## Documentation

This documentation only documents the technical usage of this package with little to no text.

- Documentation: [https://symbiont.gitlab.io/config](https://symbiont.gitlab.io/config)
- Gitlab: [https://gitlab.com/symbiont/config](https://gitlab.com/symbiont/config)

---

## Tests

```bash
composer test
```

---

## License

[MIT license](LICENSE.MD)